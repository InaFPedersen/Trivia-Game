import { createStore } from "vuex";

export default createStore({
    state: {
        //Is used: States used in the application
        user: null
    },
    mutations: {
        //Is used: Mutations is used to change the states.
        setUser: (state, user) => {
            state.user = user
        }
    },
    actions: {
        //Is used: where you can make syncronous calls for the application

    }
})